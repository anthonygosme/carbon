package com.gosme;

import com.gosme.exploration.Utilitaire;
import com.gosme.exploration.business.ChasseTresor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        ChasseTresor chasseTresor = new ChasseTresor();
        chasseTresor.fileToCarteTresor(Utilitaire.RESOURCE_FOLDER + "testin/testMainIn.txt");
        logger.info(" === vue initiale ===");
        logger.info("\n{}", chasseTresor.getCarte());
        chasseTresor.mouvements();
        logger.info(" === vue finale ===");
        logger.info("\n{}", chasseTresor.getCarte());
        logger.info("\n{}", chasseTresor);
        chasseTresor.saveChasseTresor(Utilitaire.RESOURCE_FOLDER + "testout/testMainOut.txt");
        Utilitaire.showMemory();
    }
}
