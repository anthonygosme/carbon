package com.gosme.exploration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utilitaire {
    public static final String RESOURCE_FOLDER
            = "/Users/toto/repo/gitlab/carbon/src/test/resources/";
    static final Logger logger = LoggerFactory.getLogger(Utilitaire.class);

    private Utilitaire() {
        throw new IllegalStateException("Utility class");
    }

    public static void showMemory() {
        logger.warn("Mémoire utilisé: {}Mo", (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1_000_000);
    }
}
