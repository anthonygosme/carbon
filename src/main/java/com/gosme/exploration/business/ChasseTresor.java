package com.gosme.exploration.business;

import com.gosme.exploration.entity.Aventurier;
import com.gosme.exploration.entity.Carte;
import com.gosme.exploration.entity.Constantes;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Data
@NoArgsConstructor
public class ChasseTresor {
    private static Logger logger = LoggerFactory.getLogger(ChasseTresor.class);
    private Map<Integer, Aventurier> aventuriers = new LinkedHashMap<>();
    private Carte carte;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("C").append(" - ").append(carte.getLargeur()).append(" - ").append(carte.getHauteur()).append("\n");
        montagneToString(sb);
        tresorToString(sb);
        aventurierToString(sb);
        return sb.toString();
    }

    private void montagneToString(StringBuilder sb) {
        for (Map.Entry<Integer, TreeMap<Integer, LinkedList<Character>>> LargeurEntry : carte.getCases().entrySet()) {
            Integer x = LargeurEntry.getKey();
            for (Map.Entry<Integer, LinkedList<Character>> HauteurEntry : LargeurEntry.getValue().entrySet()) {
                Integer y = HauteurEntry.getKey();
                if (HauteurEntry.getValue().get(Constantes.TYPE) == 'M')
                    sb.append('M').append(" - ").append(x).append(" - ").append(y).append("\n");
            }
        }
    }

    private void tresorToString(StringBuilder sb) {
        for (Map.Entry<Integer, TreeMap<Integer, LinkedList<Character>>> LargeurEntry : carte.getCases().entrySet()) {
            Integer x = LargeurEntry.getKey();
            for (Map.Entry<Integer, LinkedList<Character>> HauteurEntry : LargeurEntry.getValue().entrySet()) {
                Integer y = HauteurEntry.getKey();
                int nbtTresor = 0;
                for (Character iter : HauteurEntry.getValue())
                    if (iter.equals('T'))
                        nbtTresor++;
                if (nbtTresor > 0)
                    sb.append('T').append(" - ").append(x).append(" - ").append(y).append(" - ").append(nbtTresor).append("\n");
            }
        }
    }

    void aventurierToString(StringBuilder sb) {
        for (Map.Entry<Integer, Aventurier> entry : aventuriers.entrySet()) {
            Aventurier av = entry.getValue();
            sb.append('A').append(" - ").append(av.getNom()).append(" - ").append(av.getX())
                    .append(" - ").append(av.getY()).append(" - ").append(av.getOrientation())
                    .append(" - ").append(av.getNbTresor()).append("\n");
        }
    }

    public void mouvements() {
        boolean mouvRestantTotalAventurier;
        Mouvement[] mouvements = new Mouvement[aventuriers.size()];
        for (int iAventurier = 0; iAventurier < aventuriers.size(); iAventurier++)
            mouvements[iAventurier] = new Mouvement(carte, aventuriers.get(iAventurier));
        do {
            mouvRestantTotalAventurier = false;
            for (int iAventurier = 0; iAventurier < aventuriers.size(); iAventurier++) {
                boolean avMouvRestant = mouvements[iAventurier].nextMove();
                mouvRestantTotalAventurier = mouvRestantTotalAventurier || avMouvRestant;
                logger.debug("\n{}", getCarte());
            }
        } while (mouvRestantTotalAventurier);
    }

    public void saveChasseTresor(String file) throws IOException {
        Files.write(Paths.get(file), this.toString().getBytes());
    }

    public void fileToCarteTresor(String file) throws IOException {

        final List<String> lines = Files.readAllLines(Paths.get(file));
        int x;
        int y;
        int numAventurier = 0;
        for (String line : lines) {
            String[] args = line.split(" - ");
            switch (args[Constantes.TYPE]) {
                case "C":
                    carte = new Carte(Integer.parseInt(args[Constantes.LARGEUR]), Integer.parseInt(args[Constantes.HAUTEUR]));
                    break;
                case "T":
                    x = Integer.parseInt(args[Constantes.X]);
                    y = Integer.parseInt(args[Constantes.Y]);
                    int nb = Integer.parseInt(args[Constantes.TRESOR_NB]);
                    carte.setTresor(x, y, nb);
                    break;
                case "M":
                    x = Integer.parseInt(args[Constantes.X]);
                    y = Integer.parseInt(args[Constantes.Y]);
                    carte.setMontagne(x, y);
                    break;
                case "A":
                    x = Integer.parseInt(args[Constantes.AVENTURIER_X]);
                    y = Integer.parseInt(args[Constantes.AVENTURIER_Y]);
                    carte.setAventurier(x, y);
                    Aventurier av = Aventurier.builder().nom(args[Constantes.NOM]).x(x).y(y)
                            .orientation(args[Constantes.ORIENTATION].charAt(0))
                            .mouvements(args[Constantes.MOUVEMENTS]).build();
                    aventuriers.put(numAventurier++, av);
                    break;
                default:
                    break;
            }
        }
    }
}
