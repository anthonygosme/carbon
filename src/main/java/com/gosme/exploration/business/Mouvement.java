package com.gosme.exploration.business;

import com.gosme.exploration.entity.Aventurier;
import com.gosme.exploration.entity.Carte;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mouvement {
    static final Logger logger = LoggerFactory.getLogger(Mouvement.class);
    private final Carte carte;
    private final Aventurier aventurier;
    private int tourNb = 0;

    public Mouvement(Carte carte, Aventurier aventurier) {
        this.carte = carte;
        this.aventurier = aventurier;
    }

    public boolean nextMove() {
        if (tourNb >= aventurier.getMouvementsString().length()) return false;
        char action = aventurier.getMouvementsString().charAt(tourNb);
        logger.debug("tour:{} action:{}  aventurier:{}", tourNb, action, aventurier.getNom());
        if (action == 'A') avance();
        if (action == 'D') aventurier.droite();
        if (action == 'G') aventurier.gauche();
        tourNb++;
        return true;
    }

    void avance() {
        int nextX = aventurier.getX();
        int nextY = aventurier.getY();
        switch (aventurier.getOrientation()) {
            case 'N':
                nextY--;
                break;
            case 'S':
                nextY++;
                break;
            case 'E':
                nextX++;
                break;
            case 'W':
                nextX--;
                break;
            default:
                logger.error("erreur d'orientation {}", aventurier.getOrientation());
                break;
        }
        if (nextX < 0) return;
        if (nextY < 0) return;
        if (nextX >= carte.getLargeur()) return;
        if (nextY >= carte.getHauteur()) return;
        if (carte.haveObjet(nextX, nextY, 'M')) return;
        goNextPosition(nextX, nextY);
    }

    void goNextPosition(int newX, int newY) {
        carte.removeFirstAventurier(aventurier.getX(), aventurier.getY());
        aventurier.setX(newX);
        aventurier.setY(newY);
        carte.setAventurier(newX, newY);
        if (carte.haveObjet(newX, newY, 'T')) {
            aventurier.setNbTresor(aventurier.getNbTresor() + 1);
            carte.removeTresor(newX, newY);
            logger.debug("remove tresor by {}", aventurier.getNom());
        }
    }
}
