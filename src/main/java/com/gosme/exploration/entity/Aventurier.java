package com.gosme.exploration.entity;

import lombok.Builder;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data

public class Aventurier {
    private static Logger logger = LoggerFactory.getLogger(Aventurier.class);
    private String nom;
    private int x;
    private int y;
    private char orientation;
    private String mouvementsString;
    private int nbTresor = 0;

    @Builder
    public Aventurier(String nom, int x, int y, char orientation, String mouvements) {
        this.x = x;
        this.y = y;
        this.nom = nom;
        this.orientation = orientation;
        this.mouvementsString = mouvements;
    }

    @Override
    public String toString() {
        return nom + " - " + x + " - " + y + " - " + orientation + " - " + mouvementsString + " - " + nbTresor;
    }

    public void gauche() {
        switch (orientation) {
            case 'N':
                orientation = 'W';
                break;
            case 'S':
                orientation = 'E';
                break;
            case 'E':
                orientation = 'N';
                break;
            case 'W':
                orientation = 'S';
                break;
            default:
                logger.error("erreur de direction {}", orientation);
        }
    }

    public void droite() {
        switch (orientation) {
            case 'N':
                orientation = 'E';
                break;
            case 'S':
                orientation = 'W';
                break;
            case 'E':
                orientation = 'S';
                break;
            case 'W':
                orientation = 'N';
                break;
            default:
                logger.error("erreur de direction {}", orientation);
                break;
        }
    }
}
