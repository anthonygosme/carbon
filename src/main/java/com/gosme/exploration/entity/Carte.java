package com.gosme.exploration.entity;


import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

@Data
public class Carte {
    private static Logger logger = LoggerFactory.getLogger(Carte.class);
    TreeMap<Integer, TreeMap<Integer, LinkedList<Character>>> cases;
    int largeur;
    int hauteur;

    public Carte(int largeur, int hauteur) {
        cases = new TreeMap<>();
        this.largeur = largeur;
        this.hauteur = hauteur;
    }

    public String toString() {
        StringBuilder strCarte = new StringBuilder();
        for (int h = 0; h < hauteur; h++) {
            for (int l = 0; l < largeur; l++)
                strCarte.append(get(l, h)).append(" ");
            strCarte.append("\n");
        }
        return strCarte.toString();
    }

    public List<Character> get(int x, int y) {
        if ((cases.get(x) == null) || (cases.get(x).get(y) == null)) return Collections.emptyList();
        return cases.get(x).get(y);
    }

    public void removeFirstAventurier(int x, int y) {
        List<Character> list = get(x, y);
        list.remove((Character) 'A');
        if (list.isEmpty()) list.add('.');
    }

    public void setCharacter(int x, int y, Character character) {
        cases.putIfAbsent(x, new TreeMap<>());
        cases.get(x).putIfAbsent(y, new LinkedList<>());
        cases.get(x).get(y).add(character);
    }

    public void setCharacters(int x, int y, Character character, int nb) {
        cases.putIfAbsent(x, new TreeMap<>());
        cases.get(x).putIfAbsent(y, new LinkedList<>());
        cases.get(x).get(y).addAll(Collections.nCopies(nb, character));
    }

    public void setTresor(int x, int y, int nb) {
        setCharacters(x, y, 'T', nb);
    }

    public void setMontagne(int x, int y) {
        setCharacter(x, y, 'M');
    }

    public void setAventurier(int x, int y) {
        setCharacter(x, y, 'A');
    }

    public boolean haveObjet(int x, int y, Character c) {
        if (get(x, y) == null || get(x, y).isEmpty()) return false;
        return get(x, y).contains(c);
    }

    public void removeTresor(int x, int y) {
        List<Character> list = get(x, y);
        list.remove((Character) 'T');
        if (list.isEmpty()) list.add('.');
    }
}
