package com.gosme.exploration.entity;

public class Constantes {
    public static final byte TYPE = 0;
    public static final byte LARGEUR = 1;
    public static final byte HAUTEUR = 2;
    public static final byte X = 1;
    public static final byte Y = 2;
    public static final byte TRESOR_NB = 3;
    public static final byte NOM = 1;
    public static final byte AVENTURIER_X = 2;
    public static final byte AVENTURIER_Y = 3;
    public static final byte ORIENTATION = 4;
    public static final byte MOUVEMENTS = 5;

    private Constantes() {
        throw new IllegalStateException("Utility class");
    }
}
