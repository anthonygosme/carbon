package com.gosme;

import com.gosme.exploration.Utilitaire;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MainTest {
    @Test
    void testMain() throws IOException {
        Files.deleteIfExists(Paths.get(Utilitaire.RESOURCE_FOLDER + "testout/testMainOut.txt"));
        Main.main(null);
        assertEquals(
                Files.readAllLines(Paths.get(Utilitaire.RESOURCE_FOLDER + "testproof/testMainProof.txt")),
                Files.readAllLines(Paths.get(Utilitaire.RESOURCE_FOLDER + "testout/testMainOut.txt")));
        Utilitaire.showMemory();
    }
}
