package com.gosme;


import com.gosme.exploration.Utilitaire;
import com.gosme.exploration.business.ChasseTresor;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestBDD {
    static ChasseTresor chasseTresor;

    @Given("la carte d'entrée suivante")
    public void la_carte_d_entree_suivante(String docString) throws IOException {
        chasseTresor = new ChasseTresor();
        assertNotNull(chasseTresor);
        Files.write(Paths.get(Utilitaire.RESOURCE_FOLDER + "testin/testBddIn.txt"), docString.getBytes());
    }

    @When("j'effectue l'exploration de la carte d'entrée")
    public void j_effectue_l_exploration_de_la_carte_d_entree() throws IOException {
        assertNotNull(chasseTresor);
        Files.deleteIfExists(Paths.get(Utilitaire.RESOURCE_FOLDER + "testout/testBddOut.txt"));
        chasseTresor.fileToCarteTresor(Utilitaire.RESOURCE_FOLDER + "testin/testBddIn.txt");
        chasseTresor.mouvements();
    }

    @Then("la carte de sortie d'exploration est la suivante")
    public void la_carte_de_sortie_d_exploration_est_la_suivante(String docString) throws IOException {
        assertNotNull(chasseTresor);
        chasseTresor.saveChasseTresor(Utilitaire.RESOURCE_FOLDER + "testout/testBddOut.txt");
        String carteOut = String.join("\n",
                Files.readAllLines(Paths.get(Utilitaire.RESOURCE_FOLDER + "testout/testBddOut.txt")));
        assertEquals(docString, carteOut);
        Utilitaire.showMemory();
    }
}
