package com.gosme;

import com.gosme.exploration.Utilitaire;
import com.gosme.exploration.business.ChasseTresor;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestSuite {

    @Test
    void testPrincipal() throws IOException {
        ChasseTresor chasseTresor = new ChasseTresor();
        Files.deleteIfExists(Paths.get(Utilitaire.RESOURCE_FOLDER + "testout/testMainOut.txt"));
        chasseTresor.fileToCarteTresor(Utilitaire.RESOURCE_FOLDER + "testin/testMainIn.txt");
        chasseTresor.mouvements();
        chasseTresor.saveChasseTresor(Utilitaire.RESOURCE_FOLDER + "testout/testMainOut.txt");
        assertEquals(
                Files.readAllLines(Paths.get(Utilitaire.RESOURCE_FOLDER + "testproof/testMainProof.txt")),
                Files.readAllLines(Paths.get(Utilitaire.RESOURCE_FOLDER + "testout/testMainOut.txt")));
        Utilitaire.showMemory();
    }


}

