package com.gosme.exploration.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AventurierTest {
    @Test
    void testToString() {
        Aventurier aventurier = new Aventurier("Lara", 1, 1, 'S', "AADADAGGA");
        assertEquals("Lara - 1 - 1 - S - AADADAGGA - 0", aventurier.toString());
    }
}
