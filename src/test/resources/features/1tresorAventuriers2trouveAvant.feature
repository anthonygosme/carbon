Feature: Effectuer l'exploration d'un carte

  Scenario: l'aventurier 2 arrive en premier au trésor
    Given la carte d'entrée suivante
      """
      C - 4 - 1
      T - 2 - 0 - 1
      A - av1 - 0 - 0 - E - AAA
      A - av2 - 3 - 0 - W - AAA
      """
    When j'effectue l'exploration de la carte d'entrée
    Then la carte de sortie d'exploration est la suivante
      """
      C - 4 - 1
      A - av1 - 3 - 0 - E - 0
      A - av2 - 0 - 0 - W - 1
      """
