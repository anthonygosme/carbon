Feature: Effectuer l'exploration d'un carte

  Scenario: 2 aventuriers sur la même case et même chemin
    Given la carte d'entrée suivante
      """
      C - 3 - 1
      T - 1 - 0 - 1
      A - av1 - 0 - 0 - E - AA
      A - av2 - 0 - 0 - E - AA
      """
    When j'effectue l'exploration de la carte d'entrée
    Then la carte de sortie d'exploration est la suivante
      """
      C - 3 - 1
      A - av1 - 2 - 0 - E - 1
      A - av2 - 2 - 0 - E - 0
      """
