Feature: Effectuer l'exploration d'un carte

  Scenario: 2 aventurier bloqués par la montagne
    Given la carte d'entrée suivante
      """
      C - 5 - 1
      M - 2 - 0
      A - av1 - 0 - 0 - E - AAAA
      A - av2 - 4 - 0 - W - AAAA
      """
    When j'effectue l'exploration de la carte d'entrée
    Then la carte de sortie d'exploration est la suivante
      """
      C - 5 - 1
      M - 2 - 0
      A - av1 - 1 - 0 - E - 0
      A - av2 - 3 - 0 - W - 0
      """
