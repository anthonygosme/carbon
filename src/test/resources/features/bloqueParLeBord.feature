Feature: Effectuer l'exploration d'un carte

  Scenario: 2 aventurier bloqués par les bords
    Given la carte d'entrée suivante
      """
      C - 3 - 1
      A - av1 - 0 - 0 - E - AAADA
      A - av2 - 2 - 0 - W - AAADA
      """
    When j'effectue l'exploration de la carte d'entrée
    Then la carte de sortie d'exploration est la suivante
      """
      C - 3 - 1
      A - av1 - 2 - 0 - S - 0
      A - av2 - 0 - 0 - N - 0
      """
