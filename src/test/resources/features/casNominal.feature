Feature: Effectuer l'exploration d'un carte

  Scenario: vérifier la validité d'une carte après l'exploration
    Given la carte d'entrée suivante
      """
      C - 3 - 4
      M - 1 - 0
      M - 2 - 1
      T - 0 - 3 - 2
      T - 1 - 3 - 3
      A - Lara - 1 - 1 - S - AADADAGGA
      """
    When j'effectue l'exploration de la carte d'entrée
    Then la carte de sortie d'exploration est la suivante
      """
      C - 3 - 4
      M - 1 - 0
      M - 2 - 1
      T - 1 - 3 - 2
      A - Lara - 0 - 3 - S - 3
      """
