Feature: Effectuer l'exploration d'un carte

  Scenario: 1 millions de cases
    Given la carte d'entrée suivante
      """
      C - 1000000 - 1000000
      """
    When j'effectue l'exploration de la carte d'entrée
    Then la carte de sortie d'exploration est la suivante
      """
      C - 1000000 - 1000000
      """
