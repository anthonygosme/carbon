Feature: Effectuer l'exploration d'un carte

  Scenario: l'aventurier 1 s'arrête le 2 va jusqu'au trésor
    Given la carte d'entrée suivante
      """
      C - 4 - 1
      T - 3 - 0 - 1
      A - av1 - 0 - 0 - E - A
      A - av2 - 0 - 0 - E - AAA
      """
    When j'effectue l'exploration de la carte d'entrée
    Then la carte de sortie d'exploration est la suivante
      """
      C - 4 - 1
      A - av1 - 1 - 0 - E - 0
      A - av2 - 3 - 0 - E - 1
      """
