Feature: Effectuer l'exploration d'un carte

  Scenario: poursuivant arrivant dans les mêmes cases
    Given la carte d'entrée suivante
      """
      C - 5 - 1
      T - 1 - 0 - 1
      A - av1 - 0 - 0 - E - AAAADDAAAA
      A - av2 - 0 - 0 - E - AAAADDAAAA
      """
    When j'effectue l'exploration de la carte d'entrée
    Then la carte de sortie d'exploration est la suivante
      """
      C - 5 - 1
      A - av1 - 0 - 0 - W - 1
      A - av2 - 0 - 0 - W - 0
      """
